package alopatindev.bughunter

import android.app.Activity

trait ActivityUtils <: Activity {

  import android.content.Context
  import android.os.{Bundle, Handler, Looper}
  import android.view.View
  import android.widget.Button

  implicit val ctx: Context = this

  def find[V <: View](id: Int): V = findViewById(id).asInstanceOf[V]  // scalastyle:ignore

  def setButtonHandler(button: Button, handler: () => Unit): Unit =
    button setOnClickListener {
      new View.OnClickListener() {
        override def onClick(v: View) = handler()
      }
    }

  lazy val uiHandler = new Handler(Looper.getMainLooper)
  lazy val uiThread = Looper.getMainLooper.getThread

  def runOnHandler(handler: Handler, f: => Unit): Unit = {
    val runnable = new Runnable() {
      override def run() = f
    }
    handler post runnable
  }

  def runOnUIThread(f: => Unit): Unit = runOnHandler(uiHandler, f)

  def runAsync(f: => Unit): Unit = {
    val runnable = new Runnable() {
      override def run() = f
    }
    new Thread(runnable).start()
  }

}
