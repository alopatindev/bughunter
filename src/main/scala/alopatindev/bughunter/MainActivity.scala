package alopatindev.bughunter

import android.os.Bundle
import android.app.Activity
import android.widget.{ArrayAdapter, Button, ListView, TextView}
import android.view.View

import scala.util.Try

class MainActivity extends Activity with TypedFindView with ActivityUtils {

  import java.io.BufferedReader
  import java.io.InputStream
  import java.io.InputStreamReader

  import java.net.URL
  import java.util.ArrayList

  import org.json.JSONObject

  lazy val tags = find[ListView](R.id.tags)
  lazy val tagsAdapter = new ArrayAdapter[String](this, android.R.layout.simple_list_item_1, new ArrayList[String]())
  lazy val counter = find[TextView](R.id.counter)
  lazy val remove = find[Button](R.id.remove)

  override def onCreate(bundle: Bundle): Unit = {
    Logs.logi("MainActivity.onCreate")
    super.onCreate(bundle)
    setContentView(R.layout.main)

    tags.setAdapter(tagsAdapter)

    setButtonHandler(
      find[Button](R.id.load_tags),
      () => {
        Logs.logi("load tags!")
        runAsync {
          val tags = downloadAndParseTags("https://libre.fm/2.0/?format=json&method=tag.getTopTags").zipWithIndex
          Logs.logi(s"tags = $tags")
          runOnUIThread {
            tags foreach { case (tag: String, num: Int) => tagsAdapter.add(s"${num}. ${tag}") }
          }
        }
      }
    )

    setButtonHandler(
      find[Button](R.id.count),
      () => {
        Logs.logi("count!")
        val tagsNumber =
          if (tagsAdapter.getCount() > 0) tagsAdapter.getCount() + 8
          else 0
        counter.setText(s"$tagsNumber")
      }
    )

    setButtonHandler(
      remove,
      () => {
        Logs.logi("remove!")
        tagsAdapter.clear()
        remove.setVisibility(View.GONE)
      }
    )
  }

  override def onResume(): Unit = {
    Logs.logi("MainActivity.onResume")
    super.onResume()
  }

  override def onPause(): Unit = {
    Logs.logi("MainActivity.onPause")
    super.onPause()
    throw new NullPointerException
  }

  override def onDestroy(): Unit = {
    Logs.logi("MainActivity.onDestroy")
    super.onDestroy()
  }

  private def downloadAndParseTags(url: String): List[String] = {
    Logs.logi("downloadAndParseTags started")
    val conn = new URL(url).openConnection()
    conn.connect()
    val stream = conn.getInputStream()
    val jsonText = inputStreamToString(stream)
    val json = new JSONObject(jsonText)
    val tags = json.getJSONObject("toptags").getJSONArray("tag")
    val result: List[String] =
      (0 until tags.length())
        .map { i => tags.getJSONObject(i).getString("name") }
        .toList
    Logs.logi("downloadAndParseTags finished")
    result
  }

  private def inputStreamToString(stream: InputStream): String = {
    val reader = new InputStreamReader(stream)
    val bufferedReader = new BufferedReader(reader)
    val result = Stream.continually(bufferedReader.readLine()).takeWhile(_ != null).mkString("\n")
    result
  }

}
